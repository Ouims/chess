alias chess {
  if (!$1) { echo 4 -s * /chess: please provide a nickname to play with | return }
  elseif ($$input(Choose your time control in the list or enter your own in the form "N|N2",evum,Chess game with $1,3|0,1|0,2|0,3|0,5|0,10|0,No limit)) {
    var %i $remove($!,no limit)
    hadd -m chess host 1
    hadd chess cid $cid
    hadd chess opponent $1
    hadd chess time %i
    .ctcp $1 chess PLAY %i
  }
}
menu nicklist {
  Chess
  .$iif($0 == 1 && $1 != $me,Challenge $1) : chess $1
}

ctcp ^*:chess ?*:?:{
  haltdef
  tokenize 32 $2-
  if ($1 == PLAY) .timer -h 1 0 chess_play $nick $iif($2 != $null,$chess_safe($2-))
  elseif ($cid == $hget(chess,cid)) && ($hget(chess,opponent) == $nick) {
    if ($1 == BUSY) .timer -h 1 0 noop $!input( $nick is currently playing a game,wuo,Chess) $(|) hfree chess
    elseif ($1 == DENY) .timer -h 1 0 noop $!input( $nick has declined your challenge,iuo,Chess) $(|) hfree chess
    elseif ($1 == ACCEPT) chess_start
    elseif ($1 == START) chess_start $2
    elseif ($1 == RESIGN) {
      hadd chess status resign $hget(chess,oppcolor)
      hdel chess turn
      titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces - $hget(chess,mycolor) won by resignation
    }
    elseif ($1 == MOVE) chess_move $2-
    elseif ($1 == DRAW) {

    }
    else echo 4 -a * chess $1- ?
  }
}

alias -l chess_play {
  tokenize 32 $1 $replace($2,|,$chr(32))
  if ($hget(chess)) .ctcp $1 CHESS BUSY
  else {
    flash
    if ($input($1 challenged you with $iif($2 isnum 1-, a time control of $2m $iif($3 isnum 1-,with $3 more second $+ $iif($3 isnum 2-,s) per move),no time control),vun,Chess) == $yes) {
      .ctcp $1 CHESS ACCEPT
      hadd -m chess opponent $1
      hadd chess cid $cid
      hadd chess time $2| $+ $3
      .timerchess 1 5 hfree chess
    }
    elseif ($! == $no) .ctcp $1 CHESS DENY
  }
}

alias chess_drawcase {
  tokenize 32 $1 $2 $3 $iif(!$show,$4-,$hget(chess,$1))
  drawrect -fr @chess_buf $3 0 $chess_casetopixel($1,$2) $chess_case
  if ($4) {
    drawtext -r @chess_buf $iif($5 == black,0,16777215) MS Gothic 45 $chess_casetopixel($1,$2) $gettok(♚ ♛ ♜ ♝ ♞ ♟,$findtok(king queen rook bishop knight pawn,$4,32),32)
    drawtext -r @chess_buf 0 "MS Gothic" 45 $chess_casetopixel($1,$2) $gettok(♔ ♕ ♖ ♗ ♘ ♙,$findtok(king queen rook bishop knight pawn,$4,32),32)
  }
}

alias -l chess_enpassant {
  if ($3 == pawn) && ($istok(white 2.black 7,$4 $right($1,1),46)) && ($istok(white 4.black 5,$4 $right($2,1),46)) {
    var %ll $chess_convcase($left($1,1)).tonum,%moves,%cv $chess_convcase($calc(%ll + 1)).tolet $+ $right($2,1),%cv1 $chess_convcase($calc(%ll - 1)).tolet $+ $right($2,1)
    if ($hget(chess,%cv) == pawn $5) %moves = %cv 
    if ($hget(chess,%cv1) == pawn $5) %moves = %moves %cv1
    if (%moves) {
      if ($4 == white) set -l %t $right($1,1) + 1
      else set -l %t $right($1,1) - 1
      hadd chess enpassant $+ $5 $left($1,1) $+ %t %moves
    }
  }  
}

alias -l chess_move {
  flash @chess
  var %view $hget(chess,view),%color $hget(chess,mycolor),%oppcolor $hget(chess,oppcolor),%promotion $3
  if (!$istok($chess_getvalidmove($1),$2,32)) echo 7 -s * chess: cheating, $1 $2 isn't a valid move
  tokenize 32 $1 $2 $hget(chess,$1)
  var %captured $hget(chess,$2)
  var %cn $chess_notation($1,$2,%promotion)
  hadd chess $2 $3-4
  hdel chess $1
  chess_enpassant $1 $2 $3 %oppcolor %color
  if ($gettok($hget(chess,enpassant $+ %oppcolor),1,32) == $2) {
    var %c $left($2,1) $+ $calc($right($2,1) $iif($hget(chess,oppcolor) == white,-,+) 1)   
    %captured = $hget(chess,%c)
    %cn  =  %cn $+ e.p
    hdel chess %c
    var %drawc1 chess_drawcase %c %view $chess_colorcase(%c,rgb)
  }
  if ($3 == king) && ($left($1,1) == e) && ($right($1,1) == $iif(%oppcolor == white,1,8)) && $&
    ($left($2,1) isin cg) {
    var %rook $iif($left($2,1) == c,a,h) $+ $right($1,1),%h $hget(chess,%rook),%end $iif($left($2,1) == c,d,f) $+ $right($1,1)
    hdel chess %rook
    hadd chess %end %h
    if ($left($2,1) == c) %cn = O-O-O
    else %cn = O-O
    var %drawc2 chess_drawcase %rook %view $chess_colorcase(%rook,rgb)
    var %drawc3 chess_drawcase %end %view $chess_colorcase(%end,rgb)
  }
  hdel chess enpassant $+ $hget(chess,oppcolor)
  if ($3 == rook) && ($istok($hget(chess,r_castle),$1,32)) hadd chess r_castle $remtok($hget(chess,r_castle),$1,32)
  elseif ($3 == king) && ($istok($hget(chess,k_castle),$1,32)) hadd chess k_castle $remtok($hget(chess,k_castle),$1,32)
  if (%promotion) hadd chess $2 $v1 $hget(chess,oppcolor)
  hadd chess movelist $hget(chess,movelist) $+ $1-4 %captured $+ : $+ %cn $+ @




  if ($calc($hget(chess,movelistsel) + 1) == $numtok($hget(chess,movelist),64))  {
    hinc chess movelistsel 
    var %n $numtok($hget(chess,movelist),64),%1
    if (%n <= 30) %1 = 1
    else %1 = $int($calc((%n - 30 $iif(%n & 1,+1)) / 2)) + 1
    hadd chess movelists %1    
    if ($hget(chess,myend)) { chess_drawcase $v1 %view $chess_colorcase($v1,rgb) | hdel chess myend }
    if ($hget(chess,myorig)) { chess_drawcase $v1 %view $chess_colorcase($v1,rgb) | hdel chess myorig }
    %drawc1
    %drawc2
    %drawc3
    hadd chess hisend $2
    hadd chess hisorig $1
    chess_drawcase $1 %view $chess_hisorigcolor($1)
    ; chess_drawcase $2 %view $chess_hisendcolor($2)
    var %king $hfind(chess,king %color,1,w).data

    if ($chess_isincheck(%oppcolor,%king))  chess_drawcase %king  %view 450
    else chess_drawcase %king  %view  $chess_colorcase(%king,rgb)

    var %king $hfind(chess,king %oppcolor,1,w).data

    if ($chess_isincheck(%color,%king))  {
      chess_drawcase %king  %view 450
    }
    else chess_drawcase %king  %view  $chess_colorcase(%king,rgb)

    chess_drawcase $2 %view $chess_hisendcolor($2)
  }
  hadd chess turn my
  if ($chess_ischeckmate(%color)) {
    hadd chess status checkmate %oppcolor
    hdel chess turn
    titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces - %oppcolor won by checkmate

  }
  if ($chess_isstalemate(%color)) {
    hadd chess status stalemate
    hdel chess turn
    titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces - Game is drawn by stalemate
  }
  chess_movelist
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
  drawdot @chess
}

alias -l chess_promotion {
  if ($2 == pawn) && ($istok(white 8.black 1,$3 $right($1,1),46)) {
    hadd chess promoting 1
    var %i $input(What do you fucking want for your pawn lucky bastard?,tum,Chess promotion,Queen,Queen,Rook,Bishop,Knight)
    hdel chess promoting
    if (%i) return %i
    return cancel
  }
}

alias chess_notation {
  var %c $gettok($hget(chess,$1),2,32),%ic $iif($chess_ischeck($remove(blackwhite,%c),%c,$1,$2),+),%im $iif($chess_ischeckmate($remove(blackwhite,%c),%c,$1,$2),$chr(35),%ic)
  if ($gettok($hget(chess,$1),1,32) == pawn) {
    if ($left($1,1) == $left($2,1)) return $v1 $+ $iif($hget(chess,$2),x $+ $2,$right($2,1)) $+ $iif($3,= $+ $gettok(K Q R B N,$findtok(King Queen Rook Bishop Knight,$3,32),32)) $+ %im $+ $iif($4,e.p)
    else return $v1 $+ x $+ $2 $+ $iif($3,= $+ $gettok(K Q R B N,$findtok(King Queen Rook Bishop Knight,$3,32),32)) $+ %im $iif($4,e.p)
  }
  else return $gettok(K Q R B N,$findtok(King Queen Rook Bishop Knight,$v1,32),32) $+ $iif($hget(chess,$2),x $+ $2,$2) $+ %im
}
alias chess_movelist {
  drawrect -rf @chess_buf 16777215 0 561 41 168 298
  var %n $numtok($hget(chess,movelist),64)
  var %1 $hget(chess,movelists)
  var %x 574,%y 46,%b %1,%a 1
  var %1 = $calc(%1 * 2 -1)
  var %d  = %1 + 29
  while (%1 <= %d) {
    if ($gettok($hget(chess,movelist),%1,64) == $null) break
    if (%a & 1) {
      drawtext -r @chess_buf 0 verdana 15 $iif(%b > 9,$calc(%x - 10),%x) %y %b $+ .
      drawtext -rb @chess_buf 0 $iif(%1 == $hget(chess,movelistsel),$color(6),16777215) verdana 15 $iif(%b > 9,$calc(%x + 20),$calc(%x + 20)) %y $gettok($gettok($hget(chess,movelist),%1,64),2,58)
    }
    else  drawtext -rb @chess_buf 0 $iif(%1 == $hget(chess,movelistsel),$color(6),16777215) verdana 15 %x %y $gettok($gettok($hget(chess,movelist),%1,64),2,58)
    inc %1
    inc %a
    %x = 668
    if (%a & 1) {
      inc %y 19
      inc %b
      %x = 574
    }
  }
  drawtext -r @chess_buf 0 verdana 15 630 340 < $chr(160) >
}
alias -l chess_playmove {
  if ($calc($hget(chess,movelistsel)) != $numtok($hget(chess,movelist),64)) goto drawcopy
  var %t $ticks
  set -l %view $hget(chess,view)
  set -l %color $hget(chess,mycolor)
  set -l %oppcolor $hget(chess,oppcolor)
  tokenize 32 $1 $2 $hget(chess,$1)
  if ($chess_promotion($2,$3,%color) == cancel) goto drawcopy
  elseif ($v1) set -l %promotion $v1
  var %captured $hget(chess,$2)
  var %cn $chess_notation($1,$2,%promotion)
  hadd chess $2 $3-4
  hdel chess $1
  chess_deleteoldselect $1
  .ctcp $hget(chess,opponent) CHESS MOVE $1 $2 %promotion
  if ($hget(chess,hisend)) { chess_drawcase $v1 %view $chess_colorcase($v1,rgb) | hdel chess hisend }
  if ($hget(chess,hisorig)) { chess_drawcase $v1 %view $chess_colorcase($v1,rgb) | hdel chess hisorig }

  chess_enpassant $1 $2 $3 %color %oppcolor 

  if ($gettok($hget(chess,enpassant $+ %color),1,32) == $2) {
    if (%color == white) set -l %c $right($2,1) - 1
    else set -l %c $right($2,1) + 1
    %c = $left($2,1) $+ %c
    %captured = pawn $remove(whiteblack,%color) 
    %cn = %cn $+ e.p
    hdel chess %c
    chess_drawcase %c %view $chess_colorcase(%c,rgb)
  }
  if ($3 == king) && ($left($1,1) == e) && ($istok(white 1.black 8,%color $right($1,1),46)) && ($left($2,1) isin cg) {
    if ($left($2,1) == c) {
      set -l %rook a
      set -l %end d
      %cn = O-O-O
    }
    else {
      %cn = O-O
      set -l %rook h
      set -l %end f
    }
    %rook = %rook $+ $right($1,1)
    %end = %end $+ $right($1,1)
    set -l %h $hget(chess,%rook)
    hdel chess %rook
    hadd chess %end %h
    chess_drawcase %rook %view $chess_colorcase(%rook,rgb)
    chess_drawcase %end %view $chess_colorcase(%end,rgb)
  }
  hadd chess movelist $hget(chess,movelist) $+ $1-4 %captured $+ : $+ %cn $+ @
  hinc chess movelistsel 

  var %n $numtok($hget(chess,movelist),64),%1
  if (%n <= 30) %1 = 1
  else %1 = $int($calc((%n - 30 $iif(%n & 1,+1)) / 2)) + 1
  hadd chess movelists %1
  chess_movelist

  hdel chess enpassant $+ %color
  hadd chess myend $2

  hadd chess myorig $1
  hdel chess oldvalidmove

  if ($3 == rook) && ($istok($hget(chess,r_castle),$1,32)) hadd chess r_castle $remtok($hget(chess,r_castle),$1,32)
  elseif ($3 == king) && ($istok($hget(chess,k_castle),$1,32)) hadd chess k_castle $remtok($hget(chess,k_castle),$1,32)

  chess_drawcase $1 %view $chess_myorigcolor($1)
  if (%promotion) hadd chess $2 $v1 %color
  echo -s sub $calc($ticks - %t) ms

  ;highlight king in check
  var %king $hfind(chess,king %color,1,w).data
  chess_drawcase %king  %view  $chess_colorcase(%king,rgb)
  var %king $hfind(chess,king %oppcolor,1,w).data
  if ($chess_isincheck(%color,%king))  chess_drawcase %king  %view 450
  else chess_drawcase %king  %view  $chess_colorcase(%king,rgb)


  chess_drawcase $2 %view $chess_myendcolor($2)  

  hadd chess turn him


  if ($chess_ischeckmate(%oppcolor)) {
    hadd chess status checkmate %color
    hdel chess turn
    titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces - %color won by checkmate
  }

  if ($chess_isstalemate(%oppcolor)) {
    hadd chess status stalemate
    hdel chess turn
    titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces - Game is drawn by stalemate
  }
  :drawcopy
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
  drawdot @chess
  echo -s ensub $calc($ticks - %t) ms
}
alias chess_isincheck {
  set -l %a $2
  ;  tokenize
  noop $hfind(chess,& $1,0,w, if ($istok($chess_getvalidmove($1,isincheck).simple,%a,32)) [ $({) ] set -l %check 1 [ $(|) ] halt [ $(}) ] ).data
  return %check
}

alias -l chess_start {
  .timerchess off
  hadd chess mycolor $iif($1 != $null,$1,$gettok(white.black,$r(1,2),46))
  hadd chess oppcolor $iif($1 != $null,$iif($1 == black,white,black),$remove(whiteblack,$hget(chess,mycolor)))
  hadd chess view $hget(chess,mycolor)
  hadd chess mytime $gettok($hget(chess,time),1,124)
  hadd chess histime $gettok($hget(chess,time),1,124)
  hadd chess turn $iif($hget(chess,mycolor) == white,my,his)
  hadd chess status playing
  hadd chess movelists 1
  if ($1 == $null) .ctcp $hget(chess,opponent) CHESS START $hget(chess,oppcolor)
  chess_initwin
  chess_initpos
  chess_drawinfo
  chess_initpos
  chess_drawboard
  titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces.
}

menu @chess {
  mouse : chess_mouse $mouse.x $mouse.y
  sclick : chess_sclick $mouse.x $mouse.y
  uclick : chess_uclick $mouse.x $mouse.y
  rclick: chess_rclick $mouse.x $mouse.y
  leave : if (!$hget(chess,promoting)) { drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0 | drawdot @chess }
}

alias -l chess_rclick {
  if ($hget(chess,turn) == my) {
    if ($hget(chess,selectcount) == 1) {
      chess_deleteoldselect
      hdel chess select
      hdel chess oldvalidmove
      hdel chess selectcount
      drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
      drawdot @chess 
    }
  }
}

alias -l chess_uclick {
  if ($hget(chess,turn) == my) && ($mouse.key !& 16) {
    var %t $ticks
    var %p $chess_pixeltocase($1,$2),%s $hget(chess,select)
    if ($istok($hget(chess,oldvalidmove),%p,32)) chess_playmove $hget(chess,myorig) %p
    else {
      if (%s == $1-2) && (%p == $chess_pixeltocase($1,$2)) {
        hinc chess selectcount
        if ($hget(chess,selectcount) == 2) {
          chess_deleteoldselect
          hdel chess select
          hdel chess selectcount
        }
      }
      drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
      drawdot @chess 
    }
    echo -a in $calc($ticks - %t) ms
  }
}

alias -l chess_mouse {
  tokenize 32 $1 $2 $chess_flipboard $width(Flip board,verdana,15) $height(Flip board,verdana,15) $chess_resign $width(Resign,verdana,15) $height(Resign,verdana,15) $chess_draw $width(Draw,verdana,15) $height(Draw,verdana,15)
  ;drawrect -rf @chess_buf 16777215 0 $3-6
  ; drawtext -r @chess_buf $iif($inrect($1,$2,$3,$4,$5,$6),16515072,0) verdana 15 $3-4 Flip board
  ; drawrect -rf @chess_buf 16777215 0 $7-10
  ; drawtext -r @chess_buf $iif($inrect($1,$2,$7,$8,$9,$10) && $hget(chess,status) == playing,16515072,0) verdana 15 $7-8 Resign
  ; drawrect -rf @chess_buf 16777215 0 $11-14
  ; drawtext -r @chess_buf $iif($inrect($1,$2,$11,$12,$13,$14) && !$hget(chess,draw),16515072,0) verdana 15 $11-12 Draw
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
  if ($hget(chess,turn) == my) && ($mouse.key & 1) && ($inrect($1,$2,21,21,524,524)) && ($hget(chess,myorig)) chess_drag $v1
  drawdot @chess
}

alias -l chess_drag {
  tokenize 32 $1 $hget(chess,$1) $chess_case
  var %view $hget(chess,view)
  hadd chess selectcount 1
  drawrect -frn @chess $chess_myorigcolor($1) 0 $chess_casetopixel($1,%view) $4-5
  var %p  ♚ ♛ ♜ ♝ ♞ ♟
  var %p1 ♔ ♕ ♖ ♗ ♘ ♙
  drawtext -r @chess $iif($3 == black,0,16777215) "MS Gothic" 45 $calc($mouse.x - $4 / 2) $calc($mouse.y - $5 / 2) $gettok(%p,$findtok(king queen rook bishop knight pawn,$2,32),32)
  drawtext -r @chess 0 "MS Gothic" 45 $calc($mouse.x - $4 / 2) $calc($mouse.y - $5 / 2) $gettok(%p1,$findtok(king queen rook bishop knight pawn,$2,32),32)
}

alias -l chess_sclick {
  ; tokenize 32 $1 $2 $chess_flipboard $width(Flip board,verdana,15) $height(Flip board,verdana,15)
  ; if ($inrect($1,$2,$3,$4,$5,$6)) { hadd chess view $iif($hget(chess,view) == black,white,black) | chess_drawboard | chess_drawinfo }
  ; tokenize 32 $1 $2 $chess_resign $width(Resign,verdana,15) $height(Resign,verdana,15)
  ; if ($inrect($1,$2,$3,$4,$5,$6)) && ($hget(chess,status) == playing) { hadd chess status resigned | .ctcp $hget(chess,opponent) CHESS RESIGN |  hdel chess turn | titlebar @chess Chess - Nickname: $me - Opponent: $hget(chess,opponent) - You play the $hget(chess,mycolor) pieces. - You resigned   }
  ; tokenize 32 $1 $2 $chess_draw $width(Draw,verdana,15) $height(Draw,verdana,15)
  ; if ($inrect($1,$2,$3,$4,$5,$6)) && (!$hget(chess,drawing)) { hadd chess drawing 1 | .ctcp $hget(chess,opponent) CHESS DRAW }
  tokenize 32 $1-2 $chess_board $chess_margin
  ;echo -s $1-
  if ($inrect($1,$2,$5,$6,$3,$4)) {
    var %case $chess_pixeltocase($1,$2)
   ; echo -s $calc($hget(chess,movelistsel)) == $numtok($hget(chess,movelist),64)
    if ($hget(chess,turn) == my) && ($gettok($hget(chess,%case),2,32) == $hget(chess,mycolor)) && ($calc($hget(chess,movelistsel)) == $numtok($hget(chess,movelist),64)) chess_select %case
  }
  elseif ($inrect($1,$2,631,346,10,10)) && ($hget(chess,movelistsel) > 1) {
    var %t $gettok($hget(chess,movelist),$hget(chess,movelistsel),64),%c $gettok(%t,1,58),%c1 $gettok(%c,1,32),%c2 $gettok(%c,2,32),%capt $gettok(%c,5-6,32),%p $gettok(%c,3-4,32)
    var %cn $gettok(%t,2,58)
    if (e.p isin %cn) {
      var %ca $remove($gettok(%cn,2,$asc(x)),e.p),%ca $left(%ca,1) $+ $calc($right(%ca,1) $iif($hget(chess,movelistsel) & 1,-,+) 1)
      .chess_drawcase %ca $hget(chess,view) $chess_colorcase(%ca,rgb) %capt
      .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb)
    }
    elseif (%cn == O-O-O) {
      .chess_drawcase d $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(d $+ $right(%c2,1),rgb)
      .chess_drawcase a $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(a $+ $right(%c2,1),rgb) rook $replace($right(%c2,1),1,white,8,black)
      .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb) %capt
    }
    elseif (%cn == O-O) {
      .chess_drawcase f $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(f $+ $right(%c2,1),rgb)
      .chess_drawcase h $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(h $+ $right(%c2,1),rgb) rook $replace($right(%c2,1),1,white,8,black)
      .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb) %capt
    }
    else .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb) %capt
    .chess_drawcase %c1 $hget(chess,view) $chess_colorcase(%c1,rgb) %p
    hdec chess movelistsel
    var %n $hget(chess,movelistsel),%1
    if ($calc(%n / 2) == $calc($hget(chess,movelists) - 1)) {
      if (%n <= 30) %1 = 1
      else %1 = $int($calc((%n - 30 $iif(%n & 1,+1)) / 2)) + 1
      hadd chess movelists %1
    }
    chess_movelist

    ;backward highlight of old move
    var %t $gettok($hget(chess,movelist),$hget(chess,movelistsel),64),%c $gettok(%t,1,58),%c1 $gettok(%c,1,32),%c2 $gettok(%c,2,32),%capt $gettok(%c,5-6,32),%p $iif($regex($gettok(%t,2,58),=([QRNB])),$gettok(King Queen Rook Bishop Knight,$findtok(K Q R B N,$regml(1),32),32) $replace($right(%c2,1),8,white,1,black),$gettok(%c,3-4,32))
    .chess_drawcase %c1 $hget(chess,view) $iif(($hget(chess,mycolor) == white && $hget(chess,movelistsel) & 1) || ($hget(chess,mycolor) == black &&  $hget(chess,movelistsel) !& 1),$chess_myorigcolor(%c1),$chess_hisorigcolor(%c1))
    .chess_drawcase %c2 $hget(chess,view) $iif(($hget(chess,mycolor) == white && $hget(chess,movelistsel) & 1) || ($hget(chess,mycolor) == black &&  $hget(chess,movelistsel) !& 1),$chess_myendcolor(%c2),$chess_hisendcolor(%c2)) %p

    ;    var %cn $gettok(%t,2,58)    
    ;  echo -s > %cn
    ;  if (+ isin %cn) {
    ;   .chess_drawcase %king  $hget(chess,view) 450 
    ;  }
    ; chess_drawcase %king  %view  $chess_colorcase(%king,rgb)
    ; var %king $hfind(chess,king %oppcolor,1,w).data
    ; if ($chess_isincheck(%color,%king))  chess_drawcase %king  %view 450
    ; else chess_drawcase %king  %view  $chess_colorcase(%king,rgb)
  }


  elseif ($inrect($1,$2,658,346,10,10)) && ($hget(chess,movelistsel) < $numtok($hget(chess,movelist),64)) {
    var %t $gettok($hget(chess,movelist),$calc($hget(chess,movelistsel) + 1),64),%c $gettok(%t,1,58),%c1 $gettok(%c,1,32),%c2 $gettok(%c,2,32),%capt $gettok(%c,5-6,32),%p $gettok(%c,3-4,32)
    var %cn $gettok(%t,2,58)
    if (*e.p iswm %cn) {
      var %ca $remove($gettok(%cn,2,$asc(x)),e.p),%ca $left(%ca,1) $+ $calc($right(%ca,1) $iif($hget(chess,movelistsel) & 1,-,+) 1)
      .chess_drawcase %ca $hget(chess,view) $chess_colorcase(%ca,rgb)
      .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb)
    }
    elseif (%cn == O-O-O) {
      .chess_drawcase a $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(a $+ $right(%c2,1),rgb)
      .chess_drawcase d $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(d $+ $right(%c2,1),rgb) rook $replace($right(%c2,1),1,white,8,black)
    }
    elseif (%cn == O-O) {
      .chess_drawcase h $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(h $+ $right(%c2,1),rgb)
      .chess_drawcase f $+ $right(%c2,1) $hget(chess,view) $chess_colorcase(f $+ $right(%c2,1),rgb) rook $replace($right(%c2,1),1,white,8,black)
    }

    ;forward un-highlight previous move
    var %tt $gettok($hget(chess,movelist),$hget(chess,movelistsel),64),%tc $gettok(%tt,1,58),%tc1 $gettok(%tc,1,32),%tc2 $gettok(%tc,2,32),%tcapt $gettok(%c,5-6,32),%tp $iif($regex($gettok(%tt,2,58),=([QRNB])),$gettok(King Queen Rook Bishop Knight,$findtok(K Q R B N,$regml(1),32),32) $replace($right(%tc2,1),8,white,1,black),$gettok(%tc,3-4,32))
    .chess_drawcase %tc1 $hget(chess,view) $chess_colorcase(%tc1,rgb)
    .chess_drawcase %tc2 $hget(chess,view) $chess_colorcase(%tc2,rgb) %tp

    hinc chess movelistsel

    if ($regex(%cn,=([QRNB]))) {
      %p = $gettok(King Queen Rook Bishop Knight,$findtok(K Q R B N,$regml(1),32),32) $replace($right(%c2,1),8,white,1,black)
      .chess_drawcase %c2 $hget(chess,view) $iif(($hget(chess,mycolor) == white && $hget(chess,movelistsel) & 1) || ($hget(chess,mycolor) == black &&  $hget(chess,movelistsel) !& 1),$chess_myendcolor(%c2),$chess_hisendcolor(%c2)) %p
    }
    else .chess_drawcase %c2 $hget(chess,view) $chess_colorcase(%c2,rgb) %p
    .chess_drawcase %c1 $hget(chess,view) $chess_colorcase(%c1,rgb)

    ;forward highlight new move
    .chess_drawcase %c1 $hget(chess,view) $iif(($hget(chess,mycolor) == white && $hget(chess,movelistsel) & 1) || ($hget(chess,mycolor) == black &&  $hget(chess,movelistsel) !& 1),$chess_myorigcolor(%c1),$chess_hisorigcolor(%c1))
    .chess_drawcase %c2 $hget(chess,view) $iif(($hget(chess,mycolor) == white && $hget(chess,movelistsel) & 1) || ($hget(chess,mycolor) == black &&  $hget(chess,movelistsel) !& 1),$chess_myendcolor(%c2),$chess_hisendcolor(%c2)) %p

    if (e.p isin %cn) {
      var %ca $remove($gettok($v2,2,$asc(x)),e.p),%ca $left(%ca,1) $+ $calc($right(%ca,1) $iif($hget(chess,movelistsel) & 1,-,+) 1)
      .chess_drawcase %ca $hget(chess,view) $chess_colorcase(%ca,rgb)
    }

    var %n $hget(chess,movelistsel),%1

    if (%n > $calc($hget(chess,movelists) * 2 + 28)) {
      %1 = $int($calc((%n - 30 $iif(%n & 1,+1)) / 2)) + 1
      hadd chess movelists %1
    }
    chess_movelist
  }
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0

  drawdot @chess
}

alias -l chess_deleteoldvalidmove {
  var %a 1,%view $hget(chess,view)
  while ($gettok($hget(chess,oldvalidmove),%a,32)) {
    var %v $v1,%color $iif($hget(chess,hisorig) == %v,$chess_hisorigcolor(%v),$iif($hget(chess,hisend) == %v,$chess_hisendcolor(%v),$chess_colorcase(%v,rgb)))
    chess_drawcase %v %view %color
    inc %a
  }
}

alias -l chess_deleteoldselect {
  var %c $hget(chess,myorig)
  chess_drawcase %c $hget(chess,view) $chess_colorcase(%c,rgb)
  hdel chess myorig
  chess_deleteoldvalidmove
}

alias -l chess_select {
  var %view $hget(chess,view)
  if ($chess_pixeltocase( [ $replace($hget(chess,select),$chr(32),$chr(44)) ] ) != $1) hdel chess selectcount
  hadd chess select $mouse.x $mouse.y
  if ($hget(chess,myorig) != $1) {
    if ($hget(chess,myorig)) {
      tokenize 32 $1 $v1 $hget(chess,$v1)
      chess_deleteoldselect
      if ($istok($hget(chess,oldvalidmove),$1,32)) {
        chess_playmove $1 $2
        return
      }
    }
  }
  tokenize 32 $1 $hget(chess,$1)
  chess_drawcase $1 %view $chess_myorigcolor($1) 
  hadd chess myorig $1
  chess_selectvalidmove $1 %view
}

alias -l chess_selectvalidmove {
  var %a 1,%cases $chess_getvalidmove($1)
  while ($gettok(%cases,%a,32)) {
    var %v $v1,%m %m %v
    chess_drawcase %v $2 $chess_movecolor
    inc %a 
  }
  hadd chess oldvalidmove %m
}

alias chess_isvalidcase var %l $iif($prop == conv,$chess_convcase($left($1,1)).tolet,$left($1,1)) | if (%l isletter abcdefgh) && ($right($1,1) isnum 1-8) return %l $+ $v1
alias -l chess_convcase if ($1 !isnum 1-8) && ($1 !isletter abcdefgh) return | if ($prop == tolet) return $gettok(a b c d e f g h,$1,32) | return $findtok(a b c d e f g h,$1,32)
alias -l chess_winsize return 730 524
alias -l chess_board return 524 524
alias -l chess_margin return 21 21
alias -l chess_case return 60 60
alias -l chess_flipboard return $calc($gettok($chess_board,1,32) + 180) 200
alias -l chess_resign return $calc($gettok($chess_board,1,32) + 110) 200
alias -l chess_draw return $calc($gettok($chess_board,1,32) + 50) 200
alias -l chess_myorigcolor return $iif($chess_colorcase($1) == white,15323279,13678461)
alias -l chess_myendcolor return $iif($chess_colorcase($1) == white,15314027,13669466)
alias -l chess_hisorigcolor return $iif($chess_colorcase($1) == white,10614011,9034985)
alias -l chess_hisendcolor return $iif($chess_colorcase($1) == white,5960955,4381929)
alias -l chess_movecolor return 65407

alias -l chess_casetopixel {
  if ($chess_isvalidcase($1)) {
    var %ll $chess_convcase($left($1,1)).tonum - 1,%r $right($1,1) - 1
    tokenize 32 $chess_margin $chess_case $2
    var %x $1 + $calc($iif($5 == black,$calc(7 - %ll),%ll) * $3) $&
      ,%y $2 + $calc($iif($5 == white,$calc(7 - %r),%r) * $4)
    return %x %y
  }
}

alias -l chess_pixeltocase {
  tokenize 32 $1 $2 $chess_margin $chess_case
  set -l %x $int($calc(($1 - $3) / $5)) + 1
  set -l %y $int($calc(($2 - $4) / $6)) + 1
  if ($hget(chess,view) == white) set -l %y 9 - %y
  else set -l %x 9 - %x

  return $chess_convcase(%x).tolet $+ %y
}

alias -l chess_initpos {
  var %pawns a2 b2 c2 d2 e2 f2 g2 h2 a7 b7 c7 d7 e7 f7 g7 h7 $&
    ,%knights b1 g1 b8 g8,%bishops c1 f1 c8 f8,%rooks a1 h1 a8 h8 $&
    ,%queens d1 d8,%kings e1 e8,%a 1
  while ($gettok(%pawns,%a,32)) {
    var %case $v1,%color $iif(%a > 8,black,white)
    hadd chess %case pawn %color
    inc %a
  }
  %a = 1
  while ($gettok(%knights,%a,32)) {
    var %case $v1,%color $iif(%a > 2,black,white)
    hadd chess %case knight %color
    inc %a
  }
  %a = 1
  while ($gettok(%bishops,%a,32)) {
    var %case $v1,%color $iif(%a > 2,black,white)
    hadd chess %case bishop %color    
    inc %a
  }
  %a = 1
  while ($gettok(%rooks,%a,32)) {
    var %case $v1,%color $iif(%a > 2,black,white)
    hadd chess %case rook %color
    inc %a
  }
  %a = 1
  while ($gettok(%queens,%a,32)) {
    var %case $v1,%color $iif(%a > 1,black,white)
    hadd chess %case queen %color
    inc %a
  }
  %a = 1
  while ($gettok(%kings,%a,32)) {
    var %case $v1,%color $iif(%a > 1,black,white)
    hadd chess %case king %color
    inc %a
  }
  hadd chess r_castle a1 h1 a8 h8
  hadd chess k_castle e1 e8
}

alias -l chess_drawboard {
  var %view $hget(chess,view),%a 1,%b,%c,%color
  drawrect -rf @chess_buf 16777215 0 0 0 524 524
  drawrect -r @chess_buf $color(5) 1 20 20 482 482

  while ($gettok(a b c d e f g h,%a,32)) {
    %c = $v1
    var %wl 45,%wn 465
    if (%view == white) {
      drawtext -r @chess_buf 0 fixedsys 10 5 $calc(%wn - (%a -1) * 60) %a
      drawtext -r @chess_buf 0 fixedsys 10 $calc(%wl + (%a -1) * 60) 506 %c
    }
    else {
      drawtext -r @chess_buf 0 fixedsys 10 5 $calc(%wl + (%a -1) * 60) %a
      drawtext -r @chess_buf 0 fixedsys 10 $calc(%wn - (%a -1) * 60) 506 %c
    }
    %b = 1
    while (%b <= 8) {
      if ($istok($hget(chess,oldvalidmove),%c $+ %b,32)) %color = $chess_movecolor
      elseif ($hget(chess,myend) == %c $+ %b) %color = $chess_myendcolor($v1)
      elseif ($hget(chess,hisend) == %c $+ %b) %color = $chess_hisendcolor($v1)
      elseif ($hget(chess,myorig) == %c $+ %b) %color = $chess_myorigcolor($v1)
      elseif ($hget(chess,hisorig) == %c $+ %b) %color = $chess_hisorigcolor($v1)      
      else %color = $chess_colorcase(%c $+ %b,rgb)
      chess_drawcase %c $+ %b %view %color
      inc %b
    }
    inc %a
  }
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
  drawdot @chess
}

alias chess_getvalidmove {
  ; echo -s ici $2
  var %piece $gettok($hget(chess,$1),1,32),%color $gettok($hget(chess,$1),2,32), %oppcolor $remove(blackwhite,%color) $&
    ,%l $left($1,1),%r $right($1,1), %ll $chess_convcase(%l).tonum,%moves,%valid,%cv,%cv1,%cv2
  if (%piece == pawn) {
    if (%color == white) {
      %cv = $chess_convcase($calc(%ll -1)).tolet $+ $calc(%r + 1)
      %cv1 = $chess_convcase($calc(%ll +1)).tolet $+ $calc(%r + 1)
      %cv2 = %l $+ $calc(%r + 1)
      if (%r == 2) && (!$hget(chess,%l $+ 3)) && (!$hget(chess,%l $+ 4)) %moves = %l $+ 4
    }
    else {
      %cv = $chess_convcase($calc(%ll -1)).tolet $+ $calc(%r - 1)
      %cv1 = $chess_convcase($calc(%ll +1)).tolet $+ $calc(%r - 1)
      %cv2 = %l $+ $calc(%r - 1)
      if (%r == 7) && (!$hget(chess,%l $+ 6)) && (!$hget(chess,%l $+ 5)) %moves = %l $+ 5
    }
    if ($hget(chess,enpassant $+ %color)) && ($istok($gettok($v1,2-,32),$1,32))  %moves = %moves $gettok($hget(chess,enpassant $+ %color),1,32)
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) %moves = %moves %cv
    if ($gettok($hget(chess,%cv1),2,32) == %oppcolor) %moves = %moves %cv1
    if (%r isnum 2-7) && (!$hget(chess,%cv2)) %moves = %moves %cv2
  }
  elseif (%piece == knight) %moves = $chess_knightmoves(%ll,%r,%color)
  elseif (%piece == bishop) %moves = $chess_bishopmoves(%ll,%r,%oppcolor)
  elseif (%piece == rook) %moves = $chess_rookmoves(%ll,%r,%oppcolor)
  elseif (%piece == queen) %moves = $chess_bishopmoves(%ll,%r,%oppcolor) $chess_rookmoves(%ll,%r,%oppcolor)  
  elseif (%piece == king) %moves = $chess_kingmoves(%ll,%r,%color,$prop)
  if ($prop == simple) return %moves
  var %a 1
  while ($gettok(%moves,%a,32)) {
    var %v $v1
    if (!$chess_ischeck(%color,%oppcolor,$1,$v1)) %valid = %valid %v
    inc %a
  }
  return %valid
}

alias chess_kingmoves {
  set -l %1 $1-
  set -l %a 1
  set -l %moves
  set -l %cv
  set -l %color $3
  set -l %king $+($calc($1 -1),$2 $calc($1 +1),$2 $1,$calc($2 +1) $1,$calc($2 - 1) $calc($1 -1),$calc($2 -1) $calc($1 -1),$calc($2 + 1) $calc($1 +1),$calc($2 -1) $calc($1 +1),$calc($2 + 1))
  tokenize 32 %king
  scon -r set -l % $+ cv $!chess_isvalidcase( $* ).conv $(|) if (%cv) && ($gettok($hget(chess,%cv),2,32) != %color ) % $+ moves = % $+ moves % $+ cv
  tokenize 32 %1
  if ($4 != simple) {
    %cv = $chess_convcase($1).tolet $+ $2

    if ($chess_cancastle(%cv,$iif(%color == white,a1,a8),%color)) %moves = %moves $v1
    if ($chess_cancastle(%cv,$iif(%color == white,h1,h8),%color)) %moves = %moves $v1
  }
  return %moves
}

alias chess_knightmoves {
  var %a 1,%moves,%cv,%color $3,%knight $+($calc($1 -1),$calc($2 +2) $calc($1 -2),$calc($2 +1) $calc($1 -2),$calc($2 -1) $calc($1 -1),$calc($2 -2) $calc($1 +1),$calc($2 -2) $calc($1 +2),$calc($2 -1) $calc($1 +2),$calc($2 +1) $calc($1 +1),$calc($2 +2))
  tokenize 32 %knight
  scon -r set -l % $+ cv $!chess_isvalidcase( $* ).conv $(|) if (%cv) && ($gettok($hget(chess,%cv),2,32) != %color ) % $+ moves = % $+ moves % $+ cv
  return %moves
}

alias chess_rookmoves {
  var %cv,%a $1 - 1,%b $2,%moves,%oppcolor $3
  while (%a isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    dec %a
  }
  %a = $1 + 1
  %b = $2
  while (%a isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b   
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    inc %a
  }
  %a = $1
  %b = $2 + 1
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    inc %b
  }
  %a = $1
  %b = $2 - 1
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    dec %b
  }
  return %moves
}

alias chess_bishopmoves {
  var %cv,%a $1 - 1,%b $2 + 1,%moves,%oppcolor $3
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    %a = %a - 1
    %b = %b + 1
  }
  %a = $1 + 1
  %b = $2 + 1
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    %a = %a + 1
    %b = %b + 1
  }
  %a = $1 - 1
  %b = $2 - 1
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    %a = %a - 1
    %b = %b - 1
  }
  %a = $1 + 1
  %b = $2 - 1
  while (%a isnum 1-8) && (%b isnum 1-8) {
    %cv = $chess_convcase(%a).tolet $+ %b
    if ($gettok($hget(chess,%cv),2,32) == %oppcolor) { %moves = %moves %cv | break }
    elseif (!$v1) %moves = %moves %cv
    else break
    %a = %a + 1
    %b = %b - 1
  }
  return %moves
}

alias chess_ischeck {
  if ($3) && ($4) {
    set -l %temp $+($3 $hget(chess,$3),-,$4 $hget(chess,$4))
    hadd chess $4 $hget(chess,$3)
    hdel chess $3
  }
  set -l %king $hfind(chess,king $1,1,w).data
  set -l %no 0
  noop $hfind(chess,& $2,0,w, if ($istok($chess_getvalidmove($1,ischeck).simple,%king,32)) [ $({) ] %no = 1 [ $(|) ] halt [ $(}) ] ).data
  if (%temp) {
    tokenize 45 $v1
    hadd chess $1
    hadd chess $2
  }
  return %no
}

alias chess_ischeckmate {
  if ($3) && ($4) {
    set -l %temp $+($3 $hget(chess,$3),-,$4 $hget(chess,$4))
    hadd chess $4 $hget(chess,$3)
    hdel chess $3
  }
  var %color $1,%oppcolor $remove(blackwhite,$1),%mate 1
  if ($chess_ischeck(%color,%oppcolor)) noop $hfind(chess,& %color,0,w,if ($chess_getvalidmove($1,ismate)) [ $({) ] set -l %mate 0 [ $(|) ] halt  [ $(}) ] ).data
  else %mate = 0
  if (%temp) {
    tokenize 45 $v1
    hadd chess $1
    hadd chess $2
  }
  return %mate
}

alias chess_isstalemate {
  var %color $1,%oppcolor $remove(blackwhite,$1),%draw 1
  if (!$chess_ischeck(%color,%oppcolor)) {
    noop $hfind(chess,& %color,0,w,if ($chess_getvalidmove($1,isdraw)) [ $({) ] set -l %draw 0 [ $(|) ] halt [ $(}) ] ).data
    return %draw
  }
}

alias -l chess_cancastle {
  var %cases,%r $right($2,1),%a 1
  if ($1 == $iif($3 == white,e1,e8)) && ($istok($iif($3 == white,a1 h1,a8 h8),$2,32)) && ($istok($hget(chess,r_castle),$2,32)) && $&
    ($istok($hget(chess,k_castle),$1,32)) && (!$chess_ischeck($3,$remove(whiteblack,$3))) {
    if ($left($2,1) == a) {
      %cases = $+(b,%r c,%r d,%r)
    }
    elseif ($v1 == h) {
      %cases = $+(f,%r g,%r)
    }
    while (!%no) && ($gettok(%cases,%a,32)) {
      var %v $v1
      if ($hget(chess,%v)) || ($chess_ischeck($3,$remove(whiteblack,$3),$1,%v)) var %no 1
      inc %a
    }
    if (!%no) return $iif($left($2,1) == a,c,g) $+ %r
    return
  }
}

alias chess_colorcase {
  if ($2 == rgb) set -l %t 11917808 6523061
  else set -l %t white black
  tokenize 32 %t $findtok(a b c d e f g h,$left($1,1),32) $findtok(1 2 3 4 5 6 7 8,$right($1,1),32)
  if ($3 & 1) {
    if ($4 & 1) return $2
    else return $1
  }
  else {
    if ($4 & 1) return $1
    else return $2
  }
}

alias -l chess_initwin {
  window -BCoepfd +tn @chess -1 -1 $chess_winsize
  window -BChpfd +tn @chess_buf -1 -1 $chess_winsize
  drawrect -fr @chess_buf 16777215 0 0 0 $chess_winsize
  drawrect -fr @chess 16777215 0 0 0 $chess_winsize
}

alias -l chess_drawinfo {
  tokenize 32 $chess_board $chess_flipboard $width(Flip board,verdana,15) $height(Flip board,verdana,15) $&
    $chess_resign $width(Resign,verdana,15) $height(Resign,verdana,15) $&
    $chess_draw $width(Draw,verdana,15) $height(Draw,verdana,15)
  if ($hget(chess,mycolor) == white) {
    if ($hget(chess,view) == black) var %x $1 + 10 ,%y 10,%x1 $1 + 10,%y1 $2 - 30
    else var %x1 $1 + 10 ,%y1 10,%x $1 + 10,%y $2 - 30
  }
  else {
    if ($hget(chess,view) == black) var %x1 $1 + 10 ,%y1 10,%x $1 + 10,%y $2 - 30
    else var %x $1 + 10 ,%y 10,%x1 $1 + 10,%y1 $2 - 30
  }
  ;drawrect -rf @chess_buf 16777215 0 $1 0 300 40 $1 $calc($2 - 30) 300 40 $3-6
  ; drawtext -r @chess_buf 0 verdana 15 %x %y $me - $hget(chess,mytime)
  ; drawtext -r @chess_buf 0 verdana 15 %x1 %y1 $hget(chess,opponent) - $hget(chess,histime)
  ; drawtext -r @chess_buf $iif($inrect($mouse.x,$mouse.y,$3,$4,$5,$6),16515072,0) verdana 15 $3-4 Flip board
  ; drawtext -r @chess_buf $iif($inrect($mouse.x,$mouse.y,$7,$8,$9,$10) && $hget(chess,status) == playing,16515072,0) verdana 15 $7-8 Resign
  ; drawtext -r @chess_buf $iif($inrect($mouse.x,$mouse.y,$11,$12,$13,$14) && !$hget(chess,drawing),16515072,0) verdana 15 $11-12 Draw
  drawrect -rn @chess_buf 0 1 560 40 170 300 
  drawcopy -n @chess_buf 0 0 $chess_winsize @chess 0 0
  drawdot @chess
}

on *:close:@chess:if ($hget(chess)) hfree chess | window -c @chess_buf

alias -l chess_safe bset -tc &a 1 $1 | return $!utfdecode($regsubex(safe, $bvar(&a,1-) ,/(\d+)(?: |$)/g,$chr(\1)))
